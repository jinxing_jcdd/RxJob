package com.zaodei.rxjob.system.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import com.zaodei.rxjob.common.config.Global;
import com.zaodei.rxjob.system.domain.SysMenu;
import com.zaodei.rxjob.system.domain.SysUser;
import com.zaodei.rxjob.system.service.ISysMenuService;
import com.zaodei.rxjob.core.web.base.BaseController;

/**
 * 
 *@Title:首页 业务处理
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
@Controller
public class SysIndexController extends BaseController
{
    @Autowired
    private ISysMenuService menuService;

    // 系统首页
    @GetMapping("/index")
    public String index(ModelMap mmap)
    {
        // 取身份信息
        SysUser user = getSysUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenusByUser(user);
        mmap.put("menus", menus);
        mmap.put("user", user);
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "index";
    }

    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap)
    {
        mmap.put("version", Global.getVersion());
      //  return "main";
        return "forward:/job/";
    }
}
