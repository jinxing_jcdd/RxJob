package com.zaodei.rxjob.core.web.exception.user;

import com.zaodei.rxjob.core.web.exception.base.BaseException;


/**
 *@Title:用户信息异常类
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class UserException extends BaseException{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
