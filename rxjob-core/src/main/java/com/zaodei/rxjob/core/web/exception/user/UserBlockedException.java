package com.zaodei.rxjob.core.web.exception.user;

/**
 * 
 *@Title:用户锁定异常类
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class UserBlockedException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserBlockedException(String reason)
    {
        super("user.blocked", new Object[] { reason });
    }
}
