package com.zaodei.rxjob.job.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zaodei.rxjob.common.base.AjaxResult;
import com.zaodei.rxjob.core.web.base.BaseController;
import com.zaodei.rxjob.job.admin.core.model.XxlJobInfo;
import com.zaodei.rxjob.job.admin.core.model.XxlJobLogGlue;
import com.zaodei.rxjob.job.admin.core.util.I18nUtil;
import com.xxl.job.core.glue.GlueTypeEnum;
import com.zaodei.rxjob.job.dao.XxlJobInfoDao;
import com.zaodei.rxjob.job.dao.XxlJobLogGlueDao;


/**
 *@Title:任务代码
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月19日
 */
@Controller
@RequestMapping("/job/jobcode")
public class NjobCodeController  extends BaseController {
	 private String prefix = "job/jobcode";
	@Resource
	private XxlJobInfoDao xxlJobInfoDao;
	@Resource
	private XxlJobLogGlueDao xxlJobLogGlueDao;

	@RequestMapping("/{jobId}")
	public String index(Model model, @PathVariable("jobId") int jobId) {
		XxlJobInfo jobInfo = xxlJobInfoDao.loadById(jobId);
		List<XxlJobLogGlue> jobLogGlues = xxlJobLogGlueDao.findByJobId(jobId);

		if (jobInfo == null) {
			throw new RuntimeException(I18nUtil.getString("jobinfo_glue_jobid_unvalid"));
		}
		if (GlueTypeEnum.BEAN == GlueTypeEnum.match(jobInfo.getGlueType())) {
			throw new RuntimeException(I18nUtil.getString("jobinfo_glue_gluetype_unvalid"));
		}else{
			GlueTypeEnum gule =	GlueTypeEnum.match(jobInfo.getGlueType());
			model.addAttribute("glueDesc", gule.getDesc());
		}
		// Glue类型-字典
		model.addAttribute("jobInfo", jobInfo);
		model.addAttribute("jobLogGlues", jobLogGlues);
		return prefix + "/jobcode";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public AjaxResult save(Model model, int id, String glueSource, String glueRemark) {
		// valid
		if (glueRemark==null) {
			return error(I18nUtil.getString("system_please_input") + I18nUtil.getString("jobinfo_glue_remark"));
		}
		if (glueRemark.length()<4 || glueRemark.length()>100) {
			return error( I18nUtil.getString("jobinfo_glue_remark_limit"));
		}
		XxlJobInfo exists_jobInfo = xxlJobInfoDao.loadById(id);
		if (exists_jobInfo == null) {
			return  error( I18nUtil.getString("jobinfo_glue_jobid_unvalid"));
		}
		
		// update new code
		exists_jobInfo.setGlueSource(glueSource);
		exists_jobInfo.setGlueRemark(glueRemark);
		exists_jobInfo.setGlueUpdatetime(new Date());
		xxlJobInfoDao.update(exists_jobInfo);

		// log old code
		XxlJobLogGlue xxlJobLogGlue = new XxlJobLogGlue();
		xxlJobLogGlue.setJobId(exists_jobInfo.getId());
		xxlJobLogGlue.setGlueType(exists_jobInfo.getGlueType());
		xxlJobLogGlue.setGlueSource(glueSource);
		xxlJobLogGlue.setGlueRemark(glueRemark);
		xxlJobLogGlueDao.save(xxlJobLogGlue);

		// remove code backup more than 30
		xxlJobLogGlueDao.removeOld(exists_jobInfo.getId(), 30);

		return toAjax(true);
	}
}
