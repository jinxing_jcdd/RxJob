package com.zaodei.rxjob.job.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.zaodei.rxjob.common.base.AjaxResult;
import com.zaodei.rxjob.job.admin.core.model.XxlJobInfo;
import com.xxl.job.core.biz.model.ReturnT;


/**
 *@Title:
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月17日
 */
public interface NXxlJobService {
	/**
     * 
     * 
     * @param jobInfo 任务信息
     * @return 任务信息列表
     */
    public List<XxlJobInfo> selectJobInfoList(XxlJobInfo jobInfo);
    
    /**
     * trigger job
     *
     * @param id
     * @return
     */
    public XxlJobInfo loadJobInfo(int id);
    
    
    /**
     * add job, default quartz stop
     *
     * @param jobInfo
     * @return
     */
    public AjaxResult add(XxlJobInfo jobInfo);

    /**
     * update job, update quartz-cron if started
     *
     * @param jobInfo
     * @return
     */
    public AjaxResult update(XxlJobInfo jobInfo);

    /**
     * remove job, unbind quartz
     *
     * @param id
     * @return
     */
    public AjaxResult remove(int id);

    /**
     * start job, bind quartz
     *
     * @param id
     * @return
     */
    public AjaxResult start(int id);

    /**
     * stop job, unbind quartz
     *
     * @param id
     * @return
     */
    public AjaxResult stop(int id);

    /**
     * dashboard info
     *
     * @return
     */
    public Map<String,Object> dashboardInfo();

    /**
     * chart info
     *
     * @param startDate
     * @param endDate
     * @return
     */
  public ReturnT<Map<String, Object>> chartInfo(Date startDate, Date endDate);

}
