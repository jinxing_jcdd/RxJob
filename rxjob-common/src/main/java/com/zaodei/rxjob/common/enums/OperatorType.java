package com.zaodei.rxjob.common.enums;


/**
 *@Title:操作人类别
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
