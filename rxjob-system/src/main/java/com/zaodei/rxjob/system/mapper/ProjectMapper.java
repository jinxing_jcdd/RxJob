package com.zaodei.rxjob.system.mapper;

import com.zaodei.rxjob.system.domain.Project;
import com.zaodei.rxjob.system.domain.ProjectUser;

import java.util.List;	
import java.util.Map;

/**
 * 项目 数据层
 * 
 * @author wuchanghao
 * @date 2019-02-21
 */
public interface ProjectMapper 
{
	/**
     * 查询项目信息
     * 
     * @param projectId 项目ID
     * @return 项目信息
     */
	public Project selectProjectById(Long projectId);
	
	/**
     * 查询项目列表
     * 
     * @param project 项目信息
     * @return 项目集合
     */
	public List<Project> selectProjectList(Project project);
	
	/**
     * 新增项目
     * 
     * @param project 项目信息
     * @return 结果
     */
	public int insertProject(Project project);
	
	/**
     * 修改项目
     * 
     * @param project 项目信息
     * @return 结果
     */
	public int updateProject(Project project);
	
	/**
     * 删除项目
     * 
     * @param projectId 项目ID
     * @return 结果
     */
	public int deleteProjectById(Long projectId);
	
	
	
	/**
	 * 查询项目成员
	 * @param id
	 * @return
	 */
	public List<Map<String,Object>> selectUsersById(Long id);
	/**
	 * 查询项目成员Ids
	 * @param id
	 * @return
	 */
	public List<Long> selectUserIdsById(Long id);
	/**
	 * 删除项目成员
	 * @param id
	 * @return
	 */
	public int deleteProjectUser(ProjectUser projectUser);
	
	/**
	 * 新增项目成员
	 * @param id
	 * @return
	 */
	public int insertProjectUser(ProjectUser projectUser);
	
	/**
	 * 更新项目成员
	 * @param id
	 * @return
	 */
	public int updateProjectUser(ProjectUser projectUser);
	
	/**
	 * 查询项目成员
	 * @param id
	 * @return
	 */
	public Map<String,Object> selectUsersByProjectUser(ProjectUser projectUser);
	
	
	/**
     * 查询项目列表
     * 
     * @param project 项目信息
     * @return 项目集合
     */
	public List<Project> selectProjects(Project project);
}