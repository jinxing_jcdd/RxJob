package com.zaodei.rxjob.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.zaodei.rxjob.common.base.BaseEntity;


/**
 * 项目表 sys_project_user
 * 
 * @author wuchanghao
 * @date 2019-02-21
 */
public class ProjectUser extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 项目id */
	private Long projectId;
	/** 用户id */
	private Long userId;
	/** 角色id */
    private Long  roleId;
    /** 用户名 */
    private String userName;
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("projectId", getProjectId())
            .append("userId", getUserId())
            .append("roleId", getRoleId())
             .append("userName", getUserName())
            .toString();
    }

	
}
